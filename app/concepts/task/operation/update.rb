class Task::Update < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Task, :find_by)
    step Contract::Build(constant: Task::Contract::Create)
  end

  step Nested(Present)
  step Contract::Validate(key: :task)
  step Contract::Persist()
end