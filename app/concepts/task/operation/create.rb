class Task::Create < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Task, :new)
    step Contract::Build(constant: Task::Contract::Create)
  end

  step Nested(Present)
  step Contract::Validate(key: :task)
  step Contract::Persist()
end