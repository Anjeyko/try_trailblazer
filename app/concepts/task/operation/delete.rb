  class Task::Delete < Trailblazer::Operation

  step Model(Task, :find_by)
  step :delete!

  def delete!(options, model:, **)
    model.destroy
  end
end