class Task::Index < Trailblazer::Operation
  step :model!

  def model!(options, *)
    options["model"] = ::Task.all
  end
end