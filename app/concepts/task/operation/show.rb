class Task::Show < Trailblazer::Operation
  step Model(Task, :find_by)
end