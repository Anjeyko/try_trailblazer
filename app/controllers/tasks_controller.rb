class TasksController < ApplicationController

  def index
    run Task::Index
    render cell(Task::Cell::Index, result["model"])
  end

  def show
    run Task::Show
    render cell(Task::Cell::Show, result["model"])
  end

  def new
    run Task::Create::Present
    render cell(Task::Cell::New, @form)
  end

  def create
    run Task::Create do |result|
      return redirect_to tasks_path
    end

    render cell(Task::Cell::New, @form)
  end

  def edit
    run Task::Update::Present
    render cell(Task::Cell::Edit, @form)
  end

  def update
    run Task::Update do |result|
      flash[:notice] = "#{result["model"].title} has been updated"
      return redirect_to tasks_path(result["model"].id)
    end

    render cell(Task::Cell::Edit, @form)
  end

  def destroy
    run Task::Delete
    redirect_to tasks_path
  end
end
