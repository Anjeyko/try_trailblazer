require 'reform'

module Task::Contract
  class Create < Reform::Form
    include Reform::Form::ActiveModel
    include Reform::Form::ActiveModel::ModelReflections

    model :task

    properties :title, :body, presence: true
  end
end
